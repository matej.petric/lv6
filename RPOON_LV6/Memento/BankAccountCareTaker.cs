﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV6.Memento
{
    class BankAccountCareTaker
    {
        private Dictionary<string, AccountState> states;

        public BankAccountCareTaker()
        {
            states = new Dictionary<string, AccountState>();
        }

        public void StoreState(string Key, AccountState State)
        {
            states.Add(Key, State);
        }

        public AccountState GetLastState(string Key)
        {
            return states[Key];
        }
    }
}
