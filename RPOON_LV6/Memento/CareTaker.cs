﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV6.Memento
{
    class CareTaker
    {
        private Dictionary<string, Memento> states;

        public CareTaker()
        {
            states = new Dictionary<string, Memento>();
        }

        public void StoreState(string Key,Memento State)
        {
            states.Add(Key,State);
        }

        public Memento GetLastState(string Key)
        {
            return states[Key];
        }
    }
}
