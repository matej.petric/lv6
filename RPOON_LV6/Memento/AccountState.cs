﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV6.Memento
{
    class AccountState
    {
        public string ownerName { get; private set; }
        public string ownerAddress { get; private set; }
        public decimal balance { get; private set; }
        public AccountState(string ownerName, string ownerAddress, decimal balance)
        {
            this.ownerName = ownerName;
            this.ownerAddress = ownerAddress;
            this.balance = balance;
        }
    }
}
