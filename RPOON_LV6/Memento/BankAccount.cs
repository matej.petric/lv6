﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV6.Memento
{
    class BankAccount
    {
        private string ownerName;
        private string ownerAddress;
        private decimal balance;
        public BankAccount(string ownerName, string ownerAddress, decimal balance)
        {
            this.ownerName = ownerName;
            this.ownerAddress = ownerAddress;
            this.balance = balance;
        }
        public void ChangeOwnerAddress(string address)
        {
            this.ownerAddress = address;
        }
        public void UpdateBalance(decimal amount) { this.balance += amount; }
        public string OwnerName { get { return this.ownerName; } }
        public string OwnerAddress { get { return this.ownerAddress; } }
        public decimal Balance { get { return this.balance; } }
        public AccountState StoreState()
        {
            return new AccountState(ownerName, ownerAddress, balance);
        }
        public void RestoreState(AccountState previous)
        {
            ownerName = previous.ownerName;
            ownerAddress = previous.ownerAddress;
            balance = previous.balance;
        }
        public override string ToString()
        {
            return this.ownerName + ", " + "(" + this.ownerAddress +")\n" + "BALANCE:" + this.balance;
        }
    }
}
