﻿using System;
using RPOON_LV6.Memento;
using RPOON_LV6.Lanac_odgovornosti;
using System.Linq;


namespace RPOON_LV6
{
    class Program
    {
        static void Main(string[] args)
        {
            //PRVI
            Note note1 = new Note("First", "Prvi note");
            Note note2 = new Note("Second", "Drugi note");
            Note note3 = new Note("Third", "Treći note");
            Notebook notebook = new Notebook();
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);
            IAbstractIterator iterator = notebook.GetIterator();
            for (Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            {
                note.Show();
            }
            Console.WriteLine();

            //DRUGi
            Product product1 = new Product("First", 1.19);
            Product product2 = new Product("Second", 2.19);
            Product product3 = new Product("Third", 3.19);
            Box box = new Box();
            box.AddProduct(product1);
            box.AddProduct(product2);
            box.AddProduct(product3);
            IAbstractProductIterator productIterator = box.GetIterator();
            for (Product product = productIterator.First(); productIterator.IsDone == false; product = productIterator.Next())
            {
                Console.WriteLine(product);
            }
            Console.WriteLine();

            CareTaker careTaker = new CareTaker();
            ToDoItem toDoItem = new ToDoItem("Title", "Text.", DateTime.Now);
            careTaker.StoreState("FirstState", toDoItem.StoreState());
            Console.WriteLine(toDoItem);
            toDoItem.Rename("Renamed title.");
            Console.WriteLine(toDoItem);
            toDoItem.RestoreState(careTaker.GetLastState("FirstState"));
            Console.WriteLine(toDoItem);
            Console.WriteLine();

            BankAccountCareTaker accountCareTaker = new BankAccountCareTaker();
            BankAccount account = new BankAccount("Matej", "Travna 12", 250);
            Console.WriteLine(account);
            accountCareTaker.StoreState("JustOpenedAccount", account.StoreState());
            account.UpdateBalance(-150);
            Console.WriteLine(account);
            account.RestoreState(accountCareTaker.GetLastState("JustOpenedAccount"));
            Console.WriteLine(account);
            Console.WriteLine();

            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            ConsoleLogger consoleLogger = new ConsoleLogger(MessageType.ERROR | MessageType.WARNING);
            fileLogger.SetNextLogger(consoleLogger);
            fileLogger.Log("empty file", MessageType.WARNING);
            Console.WriteLine();

            string toCheck = "provjeri";
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringLengthChecker lengthChecker = new StringLengthChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            digitChecker.SetNext(lengthChecker);
            lengthChecker.SetNext(lowerCaseChecker);
            lowerCaseChecker.SetNext(upperCaseChecker);
            Console.WriteLine("Correct string entry: "+digitChecker.Check(toCheck));
            Console.WriteLine();

            PasswordValidator passwordValidator = new PasswordValidator(digitChecker);
            passwordValidator.SetNext(lengthChecker);
            passwordValidator.SetNext(lowerCaseChecker);
            passwordValidator.SetNext(upperCaseChecker);
            Console.WriteLine("Password is valid: "+passwordValidator.ValidatePassword(toCheck));
        }
    }
}
