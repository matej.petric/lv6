﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV6.Lanac_odgovornosti
{
    class PasswordValidator
    {
        private StringChecker handler;
        private StringChecker endOfChain;

        public PasswordValidator(StringChecker FirstLink)
        {
            handler = FirstLink;
            endOfChain = FirstLink;
        }

        public void SetNext(StringChecker Link)
        {
            endOfChain.SetNext(Link);
            endOfChain = Link;
        }

        public bool ValidatePassword(string StringToCheck)
        {
            return handler.Check(StringToCheck);
        }
    }
}
